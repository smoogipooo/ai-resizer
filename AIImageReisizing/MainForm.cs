﻿using AIFramework.Agents;
using AIFramework.Helpers;
using AIImageReisizing.Agents;
using AIImageReisizing.Agents.Filling;
using AIImageReisizing.Agents.Pathing;
using AIImageReisizing.Graphics;
using AIImageReisizing.Graphics.Primitives;
using AIImageReisizing.Slicing;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AIImageReisizing
{
    public partial class MainForm : Form
    {
        private sResizableImage newImage;
        private sImage baseImage;
        private SlicingAgent slicingAgent;

        public MainForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    if (newImage != null)
                        newImage.Dispose();
                    if (baseImage != null)
                        baseImage.Dispose();

                    baseImage = new sImage(ofd.FileName);
                    newImage = new sResizableImage(ofd.FileName);
                    slicingAgent = new RandomSlicingAgent(newImage);

                    originalImage.Image = baseImage.Image;
                    loadImage();
                }
            }
        }

        private void loadImage()
        {
            if (newImage == null)
                return;

            slicingAgent.Slice(new Point2(64, 64));
        }

        private void begin()
        {
            RulesetInterface connector = new RulesetInterface(newImage, baseImage);

            RulesetPathing pathRulset = new RulesetPathing(connector);
            RulesetFilling fillRuleset = new RulesetFilling(connector);
            pathRulset.Initialize(RulesetPathing.PathingActions);
            fillRuleset.Initialize(RulesetFilling.FillingActions);

            pathRulset.Begin();
            fillRuleset.Begin();

            int wThreads;
            int pThreads;
            ThreadPool.GetMaxThreads(out wThreads, out pThreads);
            for (int i = 0; i < wThreads - 1; i++)
            {
                Scheduler.Add(delegate
                {
                    PriorityAgent pathAgent = new PriorityAgent();
                    PriorityAgent fillAgent = new PriorityAgent();
                    pathAgent.Initialize(pathRulset);
                    fillAgent.Initialize(fillRuleset);
                    pathRulset.InitializeRandomAgent(pathAgent);
                    pathAgent.StartAgent();
                    fillAgent.StartAgent();

                    while (connector.SlicedPixels.Count > 0)
                    {
                        pathAgent.Update();
                        fillAgent.Position = pathAgent.Position;
                        fillAgent.Update();
                        Console.WriteLine(connector.SlicedPixels.Count);
                    }
                });
            }

        }

        private void splitter1_SplitterMoved(object sender, SplitterEventArgs e)
        {
            loadImage();
        }

        private void MainForm_ResizeEnd(object sender, EventArgs e)
        {
            loadImage();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            begin();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            newImage.Refresh();
            newImage.Save("test.png");
        }
    }
}
