﻿using AIFramework.Helpers;
using AIImageReisizing.Graphics.Primitives;
using AIImageReisizing.Helpers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIImageReisizing.Graphics
{
    abstract class SlicingAgent
    {
        protected sResizableImage Image;

        internal SlicingAgent(sResizableImage image)
        {
            this.Image = image;
        }

        internal SlicingAgent(sResizableImage image, Point2 size)
            : this(image)
        {
            Slice(size);
        }

        /// <summary>
        /// Generates a vertical slice in the image.
        /// </summary>
        protected abstract Point2 GenerateVSlice();

        /// <summary>
        /// Generates a horizontal slice in the image.
        /// </summary>
        protected abstract Point2 GenerateHSlice();

        internal void Slice(Point2 size)
        {
            if (Image.Size.DistanceTo(size) == 0)
                return;

            while (Image.Height < size.Y)
            {
                Point2 hSlicePoint = GenerateHSlice();
                Image.InsertRow((int)hSlicePoint.Y);
            }

            while (Image.Width < size.X)
            {
                Point2 vSlicePoint = GenerateVSlice();
                Image.InsertColumn((int)vSlicePoint.X);
            }
        }
    }
}
