﻿using AIFramework.Helpers;
using AIImageReisizing.Graphics;
using AIImageReisizing.Graphics.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIImageReisizing.Slicing
{
    class RandomSlicingAgent : SlicingAgent
    {
        internal RandomSlicingAgent(sResizableImage image)
            : base(image)
        { }

        internal RandomSlicingAgent(sResizableImage image, Point2 size)
            : base(image, size)
        { }

        protected override Point2 GenerateVSlice()
        {
            return new Point2(Program.Random.Next(0, Image.Height), 0);
        }

        protected override Point2 GenerateHSlice()
        {
            return new Point2(0, Program.Random.Next(0, Image.Width));
        }
    }
}
