﻿using AIFramework.Helpers;
using AIImageReisizing.Agents.Pathing;
using AIImageReisizing.Graphics.Primitives;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using AIImageReisizing.Helpers;

namespace AIImageReisizing.Agents
{
    class RulesetInterface
    {
        private const int NEARBY_SEARCH_DEPTH = 1;

        internal sImage Image { get; private set; }
        internal sImage OriginalImage { get; private set; }
        internal ConcurrentList<ComparablePixel> SlicedPixels = new ConcurrentList<ComparablePixel>();

        internal RulesetInterface(sImage image, sImage originalImage)
        {
            this.Image = image;
            this.OriginalImage = originalImage;

            for (int row = 0; row < image.RowCount; row++)
            {
                for (int pixel = 0; pixel < image[row].PixelCount; pixel++)
                {
                    ComparablePixel newPixel = new ComparablePixel(image[row][pixel]);
                    if (newPixel.Pixel.Colour != Color.Transparent)
                        continue;
                    sColour p = null;
                    for (int i = 1; i <= NEARBY_SEARCH_DEPTH; i++)
                    {
                        //Previous row
                        if (TryGetPixel(row - 1, pixel - i, out p))
                            newPixel.NearbyPixels.Add(new ComparablePixel(p));
                        if (TryGetPixel(row - 1, pixel + i - 1, out p))
                            newPixel.NearbyPixels.Add(new ComparablePixel(p));
                        if (TryGetPixel(row - 1, pixel + i, out p))
                            newPixel.NearbyPixels.Add(new ComparablePixel(p));
                        //Current row
                        if (TryGetPixel(row, pixel - i, out p))
                            newPixel.NearbyPixels.Add(new ComparablePixel(p));
                        if (TryGetPixel(row, pixel + i, out p))
                            newPixel.NearbyPixels.Add(new ComparablePixel(p));
                        //Next row
                        if (TryGetPixel(row + 1, pixel - i, out p))
                            newPixel.NearbyPixels.Add(new ComparablePixel(p));
                        if (TryGetPixel(row + 1, pixel - i - 1, out p))
                            newPixel.NearbyPixels.Add(new ComparablePixel(p));
                        if (TryGetPixel(row + 1, pixel + i, out p))
                            newPixel.NearbyPixels.Add(new ComparablePixel(p));
                    }
                    SlicedPixels.Add(newPixel);
                }
            }
        }

        internal bool TryGetPixel(int rowIndex, int pixelIndex, out sColour dst)
        {
            dst = null;
            if (rowIndex < 0 || rowIndex >= Image.RowCount
                || pixelIndex >= Image[rowIndex].PixelCount || pixelIndex < 0)
            {
                return false;
            }
            dst = Image[rowIndex][pixelIndex];
            return true;
        }

        internal bool TryGetComparable(Point2 position, out ComparablePixel dst)
        {
            dst = SlicedPixels.Find(p => p.Pixel.Position == position);
            return dst == null ? false : true;
        }
    }
}
