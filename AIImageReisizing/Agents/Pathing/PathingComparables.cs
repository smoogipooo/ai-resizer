﻿using AIFramework.Helpers;
using AIImageReisizing.Graphics.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIImageReisizing.Agents.Pathing
{
    class ComparablePixel : Comparable
    {
        internal sColour Pixel;
        internal List<ComparablePixel> NearbyPixels = new List<ComparablePixel>();

        internal ComparablePixel(sColour pixel)
        {
            Pixel = pixel;
        }

        public override float CompareTo(Comparable other)
        {
            ComparablePixel otherComparable = other as ComparablePixel;

            return otherComparable == null ? float.MinValue : Pixel.Position.DistanceTo(otherComparable.Pixel.Position);
        }
    }
}
