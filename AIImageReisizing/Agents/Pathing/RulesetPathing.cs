﻿using AIFramework.Agents;
using AIFramework.Helpers;
using AIFramework.Rulesets;
using AIImageReisizing.Graphics.Primitives;
using AIImageReisizing.Helpers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIImageReisizing.Agents.Pathing
{
    class RulesetPathing : Ruleset
    {
        internal static string[] PathingActions = 
        {
            "MoveLeft",
            "MoveRight",
            "MoveUp",
            "MoveDown"
        };

        private RulesetInterface connector;
        private ConcurrentList<ComparablePixel> previousPositions = new ConcurrentList<ComparablePixel>();

        internal RulesetPathing(RulesetInterface connector)
        {
            this.connector = connector;
        }

        public override void Initialize(string[] rulesetActions)
        {
            base.Initialize(rulesetActions);
            CurrentAgent.Position = findRandomPixel().Pixel.Position;
        }

        internal void InitializeRandomAgent(PriorityAgent agent)
        {
            agent.Position = findRandomPixel().Pixel.Position;
        }

        public override void Update()
        {
            if (connector.SlicedPixels.Count == 0)
                CurrentAgent.StopAgent();
            else
                base.Update();
        }

        public override float Evaluate(PriorityAgent agent, AIFramework.Actions.Action action)
        {
            Point2 newPos = positionTransform(agent, action.Type);
            ComparablePixel newSlicedPixel;

            //Check if the new pixel is in a slice
            if (!connector.TryGetComparable(newPos, out newSlicedPixel))
                return -1;

            return 1f;
        }

        public override void Perform(PriorityAgent agent, AIFramework.Actions.Action action)
        {
            Point2 newPos = positionTransform(agent, action.Type);
            ComparablePixel newSlicedPixel;

            //Check if the new pixel is in a slice
            if (!connector.TryGetComparable(newPos, out newSlicedPixel))
                newSlicedPixel = findRandomPixel();

            agent.Position = newSlicedPixel.Pixel.Position;
            previousPositions.Add(newSlicedPixel);
        }

        private ComparablePixel findRandomPixel()
        {
            ComparablePixel newPixel = null;
            while (newPixel == null || !newPixel.NearbyPixels.Any(p => p.Pixel.Colour != Color.Transparent))
                newPixel = connector.SlicedPixels[Program.Random.Next(0, connector.SlicedPixels.Count)];
            return newPixel;
        }

        private Point2 positionTransform(PriorityAgent agent, string transform)
        {
            Point2 ret = new Point2(agent.Position);
            switch (transform.ToUpper())
            {
                case "MOVELEFT":
                    ret.X -= 1;
                    break;
                case "MOVERIGHT":
                    ret.X += 1;
                    break;
                case "MOVEUP":
                    ret.Y -= 1;
                    break;
                case "MOVEDOWN":
                    ret.Y += 1;
                    break;
            }
            return ret;
        }

        public override Dictionary<string, Comparable> GetComparables()
        {
            Dictionary<string, Comparable> ret = new Dictionary<string, Comparable>(previousPositions.Count);
            for (int i = 0; i < previousPositions.Count; i++)
                ret.Add("Point" + i, previousPositions[i]);
            return ret;
        }
    }
}
