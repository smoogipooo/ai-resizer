﻿using AIFramework.Agents;
using AIFramework.Helpers;
using AIFramework.Rulesets;
using AIImageReisizing.Agents.Pathing;
using AIImageReisizing.Graphics.Primitives;
using AIImageReisizing.Helpers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIImageReisizing.Agents.Filling
{
    class RulesetFilling : Ruleset
    {
        internal static string[] FillingActions =
        {
            "CopyLeft",
            "CopyRight",
            "CopyUp",
            "CopyDown",
            "CopyAvg"
        };

        private RulesetInterface connector;
        private ConcurrentList<ComparablePixel> previousColours = new ConcurrentList<ComparablePixel>();

        internal RulesetFilling(RulesetInterface connector)
        {
            this.connector = connector;
        }

        public override float Evaluate(PriorityAgent agent, AIFramework.Actions.Action action)
        {
            sColour newPixel = getPixelAfterTransform(agent, action.Type);
            if (newPixel.Colour == Color.Transparent)
                return -1;
            float distance = newPixel.DistanceTo(getPixelAfterTransform(agent, "COPYAVG"));
            if (distance == 0)
                return 2;
            else
                return 1 + 1 / distance;
        }

        public override void Perform(PriorityAgent agent, AIFramework.Actions.Action action)
        {
            ComparablePixel currentPixel;
            if (connector.TryGetComparable(agent.Position, out currentPixel))
            {
                currentPixel.Pixel.Colour = getPixelAfterTransform(agent, action.Type).Colour;
                if (currentPixel.Pixel.Colour != Color.Transparent)
                    connector.SlicedPixels.Remove(currentPixel);
                previousColours.Add(currentPixel);
                Console.WriteLine(connector.SlicedPixels.Count);
            }
        }

        private Point2 transformPosition(PriorityAgent agent, string transform)
        {
            Point2 pixelPosition = new Point2(agent.Position);

            switch (transform.ToUpper())
            {
                case "COPYLEFT":
                    pixelPosition.X -= 1;
                    break;
                case "COPYRIGHT":
                    pixelPosition.X += 1;
                    break;
                case "COPYUP":
                    pixelPosition.Y -= 1;
                    break;
                case "COPYDOWN":
                    pixelPosition.Y += 1;
                    break;
            }

            return pixelPosition;
        }

        private sColour getPixelAfterTransform(PriorityAgent agent, string transform)
        {
            if (transform.ToUpper() == "COPYAVG")
            {
                ComparablePixel currentPixel;
                float R = 0;
                float G = 0;
                float B = 0;
                int count = 0;
                if (connector.TryGetComparable(agent.Position, out currentPixel))
                {
                    for (int i = 0; i < currentPixel.NearbyPixels.Count; i++)
                    {
                        if (currentPixel.NearbyPixels[i].Pixel.Colour == Color.Transparent)
                            continue;
                        R += currentPixel.NearbyPixels[i].Pixel.Colour.R;
                        G += currentPixel.NearbyPixels[i].Pixel.Colour.G;
                        B += currentPixel.NearbyPixels[i].Pixel.Colour.B;
                        count++;
                    }

                    if (count == 0)
                        R = G = B = 0;
                    else
                    {
                        R = R / count;
                        G = G / count;
                        B = B / count;
                    }
                }
                
                return new sColour(Color.FromArgb(255, (int)Math.Floor(R), (int)Math.Floor(G), (int)Math.Floor(B)), 
                                   agent.Position);
            }

            Point2 pixelPosition = transformPosition(agent, transform);
            sColour ret;
            if (connector.TryGetPixel((int)pixelPosition.Y, (int)pixelPosition.X, out ret))
                return new sColour(Color.FromArgb(255, ret.Colour.R, ret.Colour.G, ret.Colour.B), pixelPosition);
            return getUnderlyingPixel(agent, transform);
        }

        private sColour getUnderlyingPixel(PriorityAgent agent, string transform)
        {
            Point2 pixelPosition = transformPosition(agent, transform);

            float xTransform = connector.Image.OriginalWidth / connector.Image.Width;
            float yTransform = connector.Image.OriginalHeight / connector.Image.Height;

            pixelPosition.X *= xTransform;
            pixelPosition.Y *= yTransform;

            return connector.Image[(int)Math.Floor(pixelPosition.Y)][(int)Math.Floor(pixelPosition.X)];
        }

        public override Dictionary<string, AIFramework.Helpers.Comparable> GetComparables()
        {
            Dictionary<string, Comparable> ret = new Dictionary<string, Comparable>(previousColours.Count);
            for (int i = 0; i < previousColours.Count; i++)
                ret.Add("Colour" + i, previousColours[i]);
            return ret;
        }
    }
}
