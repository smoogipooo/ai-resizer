﻿using AIFramework.Helpers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIImageReisizing.Helpers
{
    class AIMathHelper
    {
        internal static float Clamp(float value, float minValue, float maxValue)
        {
            if (value < minValue)
                value = minValue;
            if (value > maxValue)
                value = maxValue;
            return value;
        }

        internal static bool RequiresClamping(float value, float minValue, float maxValue)
        {
            if (value < minValue || value > maxValue)
                return true;
            return false;
        }

        internal static float Lerp(float val1, float val2, float param)
        {
            return (1 - param) * val1 + param * val2;
        }

        internal static Point2 Lerp(Point2 point, Point2 direction, float param)
        {
            return point + direction * param;
        }

        internal static Point2 Lerp(Point2 point, Point2 direction, RectangleF bounds, float param)
        {
            Point2 lerped = Lerp(point, direction, param);
            return new Point2(Clamp(lerped.X, bounds.X, bounds.Right), Clamp(lerped.Y, bounds.Y, bounds.Bottom));
        }

        internal static Point2 Lerp(Point2 point, float angle, float param)
        {
            Point2 direction = new Point2((float)Math.Cos(angle), (float)Math.Sin(angle));
            return Lerp(point, direction, param);
        }

        internal static Point2 Lerp(Point2 point, float angle, RectangleF bounds, float param)
        {
            Point2 direction = new Point2((float)Math.Cos(angle), (float)Math.Sin(angle));
            return Lerp(point, direction, bounds, param);
        }
    }
}
