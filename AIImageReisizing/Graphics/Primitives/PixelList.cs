﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIImageReisizing.Graphics.Primitives
{
    class PixelList : IEnumerable<sColour>
    {
        private List<sColour> pixels = new List<sColour>();

        internal int PixelCount { get { return pixels.Count; } }

        internal PixelList()
        { }

        internal PixelList(int capacity)
        {
            pixels.Capacity = capacity;
        }

        internal void AddPixel(sColour pixel)
        {
            pixels.Add(pixel);
        }

        internal void InsertPixel(int index, sColour pixel)
        {
            if (index >= pixels.Count)
                AddPixel(pixel);
            else
                pixels.Insert(index, pixel);
        }

        internal sColour this[int i]
        {
            get { return pixels[i]; }
        }

        public IEnumerator<sColour> GetEnumerator() { return pixels.GetEnumerator(); }
        IEnumerator IEnumerable.GetEnumerator() { return GetEnumerator(); }
    }
}
