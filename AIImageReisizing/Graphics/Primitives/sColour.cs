﻿using AIFramework.Helpers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIImageReisizing.Graphics.Primitives
{
    class sColour
    {
        internal Color Colour = Color.Transparent;
        internal Point2 Position = Point2.Zero;
        internal byte[] RawBytes
        {
            get
            {
                return new[]
                {
                    Colour.B,
                    Colour.G,
                    Colour.R,
                    Colour.A
                };
            }
        }

        internal sColour(Color colour, Point2 position)
        {
            Position = position;
            Colour = colour;
        }

        internal float DistanceTo(sColour other)
        {
            return ((float)other.Colour.R).DistanceTo(Colour.R)
                    + ((float)other.Colour.G).DistanceTo(Colour.G)
                    + ((float)other.Colour.B).DistanceTo(Colour.B);
        }
    }
}
