﻿using AIFramework.Helpers;
using AIImageReisizing.Helpers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace AIImageReisizing.Graphics.Primitives
{
    class sImage : IEnumerable<PixelList>, IDisposable
    {
        private ConcurrentList<PixelList> rows = new ConcurrentList<PixelList>();
        private Bitmap image;
        internal Bitmap Image
        {
            get
            {
                if (image == null)
                    Refresh();

                return image;
            }
        }

        private int depth = 1;

        internal int RowCount { get { return rows.Count; } }
        internal int OriginalWidth { get; private set; }
        internal int OriginalHeight { get; private set; }
        internal int Width { get { return rows.Max(row => row.PixelCount); } }
        internal int Height { get { return rows.Count; } }
        internal Point2 Size { get { return new Point2(Width, Height); } }

        internal sImage(Point2 size)
        {
            rows.Capacity = Height;
            for (int y = 0; y < Height; y++)
            {
                PixelList newRow = new PixelList(Width);
                for (int x = 0; x < Width; x++)
                    newRow.AddPixel(new sColour(Color.Transparent, new Point2(x, y)));
                AddRow(newRow);
            }
        }

        internal sImage(string image)
        {
            load(image);
        }

        internal void load(string image)
        {
            Bitmap internalImage = new Bitmap(image);

            OriginalWidth = internalImage.Width;
            OriginalHeight = internalImage.Height;

            Rectangle rect = new Rectangle(0, 0, internalImage.Width, internalImage.Height);
            BitmapData imgData = internalImage.LockBits(rect, ImageLockMode.ReadOnly, internalImage.PixelFormat);

            depth = Bitmap.GetPixelFormatSize(internalImage.PixelFormat) / 8;
            int pixelCount = internalImage.Width * internalImage.Height;
            byte[] rawBytes = new byte[pixelCount * depth];
            Marshal.Copy(imgData.Scan0, rawBytes, 0, rawBytes.Length);

            rows.Capacity = internalImage.Height;
            for (int y = 0; y < internalImage.Height; y++)
            {
                PixelList newRow = new PixelList(internalImage.Width);
                for (int x = 0; x < internalImage.Width; x++)
                {
                    int index = (y * internalImage.Width + x) * depth;
                    switch (depth)
                    {
                        case 1:
                            newRow.AddPixel(new sColour(Color.FromArgb(255, rawBytes[index], rawBytes[index], rawBytes[index]), new Point2(x, y)));
                            break;
                        case 2:
                            //I don't know how 16b argb works >.>
                            throw new NotImplementedException();
                        case 3:
                            newRow.AddPixel(new sColour(Color.FromArgb(255, rawBytes[index + 2], rawBytes[index + 1], rawBytes[index]), new Point2(x, y)));
                            break;
                        case 4:
                            newRow.AddPixel(new sColour(Color.FromArgb(rawBytes[index + 3], rawBytes[index + 2], rawBytes[index + 1], rawBytes[index]), new Point2(x, y)));
                            break;
                    }
                }
                AddRow(newRow);
            }

            internalImage.UnlockBits(imgData);
        }

        internal void Refresh()
        {
            Bitmap newImage = new Bitmap(Width, Height, PixelFormat.Format32bppArgb);

            Rectangle rect = new Rectangle(0, 0, Width, Height);
            BitmapData imgData = newImage.LockBits(rect, ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb);

            IntPtr index = imgData.Scan0;
            for (int i = 0; i < rows.Count; i++)
            {
                foreach (sColour pixel in rows[i])
                {
                    Marshal.Copy(pixel.RawBytes, 0, index, pixel.RawBytes.Length);
                    index += pixel.RawBytes.Length;
                }
            }

            newImage.UnlockBits(imgData);

            if (image != null)
                image.Dispose();
            image = newImage;
        }

        internal void Save(string filePath)
        {
            Image.Save(filePath);
        }

        internal void AddRow(PixelList row)
        {
            rows.Add(row);
        }

        internal void InsertRow(int index, PixelList row)
        {
            rows.Insert(index, row);
        }

        internal PixelList this[int i]
        {
            get { return rows[i]; }
        }

        public IEnumerator<PixelList> GetEnumerator() { return rows.GetEnumerator(); }
        IEnumerator IEnumerable.GetEnumerator() { return GetEnumerator(); }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (image != null)
                    image.Dispose();
                rows.Clear();
            }
        }
    }
}
