﻿using AIFramework.Helpers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIImageReisizing.Graphics.Primitives
{
    class sResizableImage : sImage
    {
        internal sResizableImage(Point2 size)
            : base(size)
        { }

        internal sResizableImage(string image)
            : base(image)
        { }

        internal void InsertRow(int index)
        {
            PixelList newRow = new PixelList(Image.Width);
            for (int i = 0; i < Image.Width; i++)
                newRow.AddPixel(new sColour(Color.Transparent, new Point2(i, index)));

            if (index >= RowCount)
                AddRow(newRow);
            else
                InsertRow(index, newRow);
        }

        internal void InsertColumn(int index)
        {
            for (int i = 0; i < RowCount; i++)
                this[i].InsertPixel(index, new sColour(Color.Transparent, new Point2(index, i)));
        }
    }
}
